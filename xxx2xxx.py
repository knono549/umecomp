#!/usr/bin/fontforge -script
# -*- coding: utf-8 -*-
#
# Copyright (c) 2016 knono549
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
#

import inspect
import os
import sys
import traceback

import fontforge

##
# @return None
def print_usage():
    print("""usage: fontforge -script %s src dest

  - src
    input font file

  - dest
    destination file""" % os.path.basename(sys.argv[0]))
    return

##
# @param aMsg str
# @return None
def print_error(aMsg):
    sys.stderr.write(aMsg)
    frame = inspect.currentframe()
    code = frame.f_code
    sys.stderr.write(" (File \"%s\", line %d, in %s)\n" %
                       (code.co_filename, frame.f_lineno, code.co_name))
    return

##
# @param aArgv list<str>
# @return int
def main(aArgv):
    # command line arguments
    if len(aArgv) < 3:
        print_usage()
        return 0

    args = aArgv[1:]

    iname = args[0]
    oname = args[1]

    # open the font file
    ifont = None
    try:
        ifont = fontforge.open(iname)
    except:
        print_error('Could not open the input font')
        traceback.print_exc()
        return 1

    # save or generate
    if oname.endswith('.sfd'):
        ifont.save(oname)
    else:
        ifont.generate(oname)

    return 0

# execute
if __name__ == '__main__':
    exit(main(sys.argv))
