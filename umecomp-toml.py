#!/usr/bin/fontforge -script
# -*- coding: utf-8 -*-
#
# Copyright (c) 2016 knono549
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
#

import inspect
import os
import sys
import traceback

import fontforge
#import math
#import psMat

import ffutils

##
# @return None
def print_usage():
    print("""usage: fontforge -script %s src

  - src
    UmeComp font file""" % os.path.basename(sys.argv[0]))
    return

##
# @param aMsg str
# @return None
def print_error(aMsg):
    sys.stderr.write(aMsg)
    frame = inspect.currentframe()
    code = frame.f_code
    sys.stderr.write(" (File \"%s\", line %d, in %s)\n" %
                       (code.co_filename, frame.f_lineno, code.co_name))
    return

##
# @param aArgv list<str>
# @return int
def main(aArgv):
    # command line arguments
    if len(aArgv) < 2:
        print_usage()
        return 0

    args = aArgv[1:]

    i1name = args[0]

    # open font files
    i1font = None
    try:
        i1font = fontforge.open(i1name)
    except:
        print_error('Could not open input fonts')
        traceback.print_exc()
        return 1

    # output
    i1em = i1font.em
    print("[hinting]")
    print("UPM = " + str(i1em))
    print("CANONICAL_STEM_WIDTH = " + str(i1em * 0.065))
    print("CANONICAL_STEM_WIDTH_SMALL = " + str(i1em * 0.065))
    print("CANONICAL_STEM_WIDTH_DENSE = " + str(i1em * 0.065))
    print("CANONICAL_STEM_WIDTH_LARGE_ADJ = " + str(0))
    print("ABSORPTION_LIMIT = " + str(i1em * 0.06))
    print("STEM_SIDE_MIN_RISE = " + str(i1em * 0.04))
    print("STEM_SIDE_MIN_DESCENT = " + str(i1em * 0.06))
    print("STEM_CENTER_MIN_RISE = " + str(i1em * 0.04))
    print("STEM_CENTER_MIN_DESCENT = " + str(i1em * 0.06))
    print("STEM_SIDE_MIN_DIST_RISE = " + str(i1em * 0.04))
    print("STEM_SIDE_MIN_DIST_DESCENT = " + str(i1em * 0.06))
    print("PPEM_INCREASE_GLYPH_LIMIT = " + str(i1em * 0.018))
    print("BLUEZONE_BOTTOM_CENTER = " + str(i1em * -0.067))
    print("BLUEZONE_BOTTOM_LIMIT = " + str(i1em * -0.055))
    print("BLUEZONE_BOTTOM_BAR_REF = " + str(i1em * -0.055))
    print("BLUEZONE_BOTTOM_BAR_MIDDLE_SIZE = " + str(i1em * 0.016))
    print("BLUEZONE_BOTTOM_BAR_SMALL = " + str(i1em * -0.055))
    print("BLUEZONE_BOTTOM_BAR_MIDDLE = " + str(i1em * -0.055))
    print("BLUEZONE_BOTTOM_BAR_LARGE = " + str(i1em * -0.055))
    print("BLUEZONE_BOTTOM_DOTBAR_SMALL = " + str(i1em * -0.055))
    print("BLUEZONE_BOTTOM_DOTBAR_MIDDLE = " + str(i1em * -0.055))
    print("BLUEZONE_BOTTOM_DOTBAR_LARGE = " + str(i1em * -0.055))
    print("BLUEZONE_TOP_CENTER = " + str(i1em * 0.8215))
    print("BLUEZONE_TOP_LIMIT = " + str(i1em * 0.793))
    print("BLUEZONE_TOP_BAR_REF = " + str(i1em * 0.793))
    print("BLUEZONE_TOP_BAR_MIDDLE_SIZE = " + str(i1em * 0.016))
    print("BLUEZONE_TOP_BAR_SMALL = " + str(i1em * 0.793))
    print("BLUEZONE_TOP_BAR_MIDDLE = " + str(i1em * 0.793))
    print("BLUEZONE_TOP_BAR_LARGE = " + str(i1em * 0.793))
    print("BLUEZONE_TOP_DOTBAR_SMALL = " + str(i1em * 0.793))
    print("BLUEZONE_TOP_DOTBAR_MIDDLE = " + str(i1em * 0.793))
    print("BLUEZONE_TOP_DOTBAR_LARGE = " + str(i1em * 0.793))
    print("BLUEZONE_WIDTH = " + str(i1em * 0.015))
    print("SLOPE_FUZZ = " + str(i1em * 0.00005))
    print("Y_FUZZ = " + str(i1em * 0.007))
    print("")
    print("[cvt]")
    print("padding = " + str(len(i1font.cvt)))

    return 0

# execute
if __name__ == '__main__':
    exit(main(sys.argv))
