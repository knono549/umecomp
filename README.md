INTRODUCTION
============

Scripts for FontForge which make the composite font named UmeComp from
Ume, M+ and Inconsolata.

![](https://bitbucket.org/knono549/umecomp/wiki/img/screenshot.png)


REQUIREMENTS
------------

* FontForge 20120731.b  
  https://fontforge.github.io/
* GNU Make 4.1  
  https://www.gnu.org/software/make/
* ttfautohint 1.5  
  https://www.freetype.org/ttfautohint/
* ideohint 0.11.1  
  https://github.com/caryll/ideohint
* otfcc 0.6.1  
  https://github.com/caryll/otfcc

* Ume-font 670  
  http://sourceforge.jp/projects/ume-font/
* M+ OUTLINE FONTS TESTFLIGHT 062  
  http://mplus-fonts.sourceforge.jp/mplus-outline-fonts/
* Inconsolata 1.016  
  https://fonts.google.com/specimen/Inconsolata


INSTALLATION
------------

    $ tar -Jxvf umefont_xxx.tar.xz
    $ mv umefont_xxx umefont

    $ tar -Jxvf mplus-xxxxxxxxxx-xxx.tar.xz
    $ mv mplus-xxxxxxxxxx-xxx mplus

    $ unzip -d Inconsolata Inconsolata.zip

    $ make jobs=4

    $ make package
