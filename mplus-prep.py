#!/usr/bin/fontforge -script
# -*- coding: utf-8 -*-
#
# Copyright (c) 2017 knono549
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
#

import inspect
import os
import sys
import traceback

import fontforge
#import math
import psMat

##
# @return None
def print_usage():
    print("""usage: fontforge -script %s src1 src2 dest

  - src1
    M+M1 font file

  - src2
    Ume Gothic font file

  - dest
    destination sfd file""" % os.path.basename(sys.argv[0]))
    return

##
# @param aMsg str
# @return None
def print_error(aMsg):
    sys.stderr.write(aMsg)
    frame = inspect.currentframe()
    code = frame.f_code
    sys.stderr.write(" (File \"%s\", line %d, in %s)\n" %
                       (code.co_filename, frame.f_lineno, code.co_name))
    return

##
# @param aArgv list<str>
# @return int
def main(aArgv):
    # command line arguments
    if len(aArgv) < 4:
        print_usage()
        return 0

    args = aArgv[1:]

    i1name = args[0]
    i2name = args[1]
    oname = args[2]

    # open font files
    i1font = None
    i2font = None
    try:
        i1font = fontforge.open(i1name)
        i2font = fontforge.open(i2name)
    except:
        print_error('Could not open input fonts')
        traceback.print_exc()
        return 1

    # tweak Exclamation mark and Yen sign
    i1font.selection.select(('ranges', None), 0x21, 0xA5)
    for glyph in i1font.selection.byGlyphs:
        glyph.transform(psMat.translate(0, 18), ('partialRefs'))

    # modify Tilde
    i1font.selection.select(0x7E)
    for glyph in i1font.selection.byGlyphs:
        glyph.transform(psMat.translate(0, 360), ('partialRefs'))

    # use Yen sign for Backslash
    i1font.selection.select(0xa5)
    i1font.copy()
    i1font.selection.select(0x5c)
    i1font.paste()

    # set the same em size as Ume
    i1font.em = i2font.em

    # tweak vertical positions of glyphs
    glyphs = i1font.glyphs()
    try:
        while True:
            glyph = glyphs.next()
            glyph.transform(psMat.translate(0, -50), ('partialRefs'))
    except StopIteration:
        pass

    # save
    i1font.save(oname)

    return 0

# execute
if __name__ == '__main__':
    exit(main(sys.argv))
