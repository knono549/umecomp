#
# Copyright (c) 2016-2017 knono549
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
#

jobs=1
srcdir=.

version:=$(shell date +%Y.%m.%d)
packagedir=$(umecompname)-fonts-$(version)
workdir=work

inconsolatadir=Inconsolata
inconsolataname=Inconsolata-Regular
umedir=umefont
umename=ume-tgo4
mplusdir=mplus
mplusname=mplus-1m-regular
umeplusname=umeplus-gothic
umecompname=umecomp

pnums=0 1 2 3 4 5 6 7 8 9


.PHONY: all clean package


all:
	-if [ ! -d $(workdir) ]; then mkdir $(workdir); fi
	cd $(workdir) && \
	  $(MAKE) -f ../Makefile -j $(jobs) srcdir=.. $(umecompname).ttf

$(inconsolataname).sfd: $(srcdir)/$(inconsolatadir)/$(inconsolataname).ttf \
                        $(srcdir)/xxx2xxx.py
	fontforge -script $(srcdir)/xxx2xxx.py $< $@

$(inconsolataname)_tmp.sfd: $(inconsolataname).sfd \
                            $(umeplusname).sfd \
                            $(srcdir)/inconsolata-prep.py
	fontforge -script $(srcdir)/inconsolata-prep.py \
	  $(inconsolataname).sfd $(umeplusname).sfd $@

$(mplusname).sfd: $(srcdir)/$(mplusdir)/$(mplusname).ttf $(srcdir)/xxx2xxx.py
	fontforge -script $(srcdir)/xxx2xxx.py $< $@

$(mplusname)_tmp.sfd: $(mplusname).sfd $(umename).sfd $(srcdir)/mplus-prep.py
	fontforge -script $(srcdir)/mplus-prep.py \
	  $(mplusname).sfd $(umename).sfd $@

$(umename).sfd: $(srcdir)/$(umedir)/$(umename).ttf $(srcdir)/xxx2xxx.py
	fontforge -script $(srcdir)/xxx2xxx.py $< $@

$(umeplusname).sfd: $(mplusname)_tmp.sfd $(umename).sfd $(srcdir)/umeplus-comp.py
	fontforge -script $(srcdir)/umeplus-comp.py \
	  $(umename).sfd $(mplusname)_tmp.sfd $@

$(umecompname)_tmp_nohint.sfd: $(inconsolataname)_tmp.sfd \
                               $(umeplusname).sfd \
                               $(srcdir)/umecomp-comp.py
	fontforge -script $(srcdir)/umecomp-comp.py \
	  $(umeplusname).sfd $(inconsolataname)_tmp.sfd $(version) $@

$(umecompname)_tmp_nohint.ttf: $(umecompname)_tmp_nohint.sfd \
                               $(srcdir)/xxx2xxx.py
	fontforge -script $(srcdir)/xxx2xxx.py $< $@

$(umecompname)_tmp_hint.ttf: $(umecompname)_tmp_nohint.ttf \
                             $(srcdir)/umecomp.ctrl
	ttfautohint -n -W -w gGD -f latn -H 266 -m $(srcdir)/umecomp.ctrl $< $@

$(umecompname)_tmp_hint.otd: $(umecompname)_tmp_hint.ttf
	otfccdump $< -o $@

$(umecompname)_tmp_hint.hgl: $(umecompname)_tmp_hint.otd
	ideohint otd2hgl $< -o $@ --ideo-only

$(umecompname)_tmp_hint.toml: $(umecompname)_tmp_hint.ttf \
                              $(srcdir)/umecomp-toml.py
	fontforge -script $(srcdir)/umecomp-toml.py \
	  $(umecompname)_tmp_hint.ttf > $@

$(umecompname)_tmp_hint.hgf: $(umecompname)_tmp_hint.hgl \
                             $(umecompname)_tmp_hint.toml
	ideohint extract $< -o $@ --parameters $(umecompname)_tmp_hint.toml

define hgi_parallel_tmpl
$(1)_part$(2).hgi: $(1).hgf $(1).toml
	ideohint hint -d $(3) -m $(2) $(1).hgf -o $(1)_part$(2).hgi \
	  --parameters $(1).toml
endef
$(foreach pnum,$(pnums), $(eval $(call \
  hgi_parallel_tmpl,$(umecompname)_tmp_hint,$(pnum),$(words $(pnums)))))

$(umecompname)_tmp_hint.hgi: $(foreach pnum,$(pnums), \
                               $(umecompname)_tmp_hint_part$(pnum).hgi)
	ideohint merge -o $@ $+

$(umecompname)_tmp.otd: $(umecompname)_tmp_hint.hgi \
                        $(umecompname)_tmp_hint.otd \
                        $(umecompname)_tmp_hint.toml
	ideohint apply $< $(umecompname)_tmp_hint.otd -o $@ \
	  --parameters $(umecompname)_tmp_hint.toml

$(umecompname)_tmp.ttf: $(umecompname)_tmp.otd
	otfccbuild $< -o $@

$(umecompname).sfd: $(umecompname)_tmp.ttf $(srcdir)/xxx2xxx.py
	fontforge -script $(srcdir)/xxx2xxx.py $< $@

$(umecompname).ttf: $(umecompname).sfd $(srcdir)/xxx2xxx.py
	fontforge -script $(srcdir)/xxx2xxx.py $< $@

clean:
	-rm -f $(workdir)/*

package:
	mkdir -p $(packagedir)/docs-ume
	mkdir -p $(packagedir)/docs-mplus
	mkdir -p $(packagedir)/docs-inconsolata
	cd $(packagedir) && \
	  cp ../$(workdir)/$(umecompname).ttf . && \
	  cp ../$(umedir)/license.html ./docs-ume && \
	  cp ../$(mplusdir)/LICENSE_E ./docs-mplus && \
	  cp ../$(mplusdir)/LICENSE_J ./docs-mplus && \
	  cp ../$(mplusdir)/README_E ./docs-mplus && \
	  cp ../$(mplusdir)/README_J ./docs-mplus && \
	  cp ../$(inconsolatadir)/OFL.txt ./docs-inconsolata && \
	  cp ../docs/COPYING . && \
	  cp ../docs/README.md .
	tar -Jcvf $(packagedir).tar.xz $(packagedir)
	rm -r $(packagedir)
