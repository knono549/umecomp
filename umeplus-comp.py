#!/usr/bin/fontforge -script
# -*- coding: utf-8 -*-
#
# Copyright (c) 2017 knono549
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
#

import inspect
import os
import sys
import traceback

import fontforge
#import math
#import psMat

##
# @return None
def print_usage():
    print("""usage: fontforge -script %s src1 src2 dest

  - src1
    Ume Gothic font file

  - src2
    M+M1 font file

  - dest
    destination file""" % os.path.basename(sys.argv[0]))
    return

##
# @param aMsg str
# @return None
def print_error(aMsg):
    sys.stderr.write(aMsg)
    frame = inspect.currentframe()
    code = frame.f_code
    sys.stderr.write(" (File \"%s\", line %d, in %s)\n" %
                       (code.co_filename, frame.f_lineno, code.co_name))
    return

##
# @param aArgv list<str>
# @return int
def main(aArgv):
    # command line arguments
    if len(aArgv) < 4:
        print_usage()
        return 0

    args = aArgv[1:]

    i1name = args[0]
    i2name = args[1]
    oname = args[2]

    # open font files
    i1font = None
    i2font = None
    try:
        i1font = fontforge.open(i1name)
        i2font = fontforge.open(i2name)
    except:
        print_error('Could not open input fonts')
        traceback.print_exc()
        return 1

    # use Parallel to for Double vertical line
    i1font.selection.select(0x2225)
    i1font.copy()
    i1font.selection.select(0x2016)
    i1font.paste()

    # import M+1M
    i2font.selection.none()
    i2font.selection.select(('ranges', 'more'), 0x20, 0x4DFF)
    i2font.selection.select(('ranges', 'more'), 0xFB00, 0xFFEF)
    # exclude some ambiguous glyphs
    branges = [(0x80, 0xFF),   # Latin-1 Supplement
               (0x100, 0x17F), # Latin Extended-A
               (0x180, 0x24F), # Latin Extended-B
               (0x370, 0x3FF), # Greek and Coptic
               (0x400, 0x4FF)] # Cyrillic
    for brange in branges:
        for enc in range(brange[0], brange[1] + 1):
            i2glyph = None
            i1glyph = None
            try:
                i2glyph = i2font[enc]
                i1glyph = i1font[enc]
            except:
                continue
            if i2glyph.width == i1glyph.width:
                continue
            i2font.selection.select(('less', None), enc)
    # exclude some blocks
    # Combining Diacritical Marks
    i2font.selection.select(('ranges', 'less'), 0x300, 0x36F)
    # Letterlike Symbols
    i2font.selection.select(('ranges', 'less'), 0x2100, 0x214F)
    # Number Forms
    i2font.selection.select(('ranges', 'less'), 0x2150, 0x218F)
    # Arrows
    i2font.selection.select(('ranges', 'less'), 0x2190, 0x21FF)
    # Mathematical Operators
    i2font.selection.select(('ranges', 'less'), 0x2200, 0x22FF)
    # Box Drawing
    i2font.selection.select(('ranges', 'less'), 0x2500, 0x257F)
    # Block Elements
    i2font.selection.select(('ranges', 'less'), 0x2580, 0x259F)
    # Geometric Shapes
    i2font.selection.select(('ranges', 'less'), 0x25A0, 0x25FF)
    # Miscellaneous Symbols
    i2font.selection.select(('ranges', 'less'), 0x2600, 0x26FF)
    # Miscellaneous Symbols and Arrows
    i2font.selection.select(('ranges', 'less'), 0x2B00, 0x2BFF)
    # Variation Selectors
    i2font.selection.select(('ranges', 'less'), 0xFE00, 0xFE0F)
    # exclude some glyphs
    # Combining Diacritical Marks for Symbols
    for enc in range(0x2000, 0x206F + 1):
        # Tironian sign et, Swung dash
        if enc == 0x204A or enc == 0x2053: continue
        i2font.selection.select(('less', None), enc)
    # Miscellaneous Technical
    for enc in range(0x2300, 0x23FF + 1):
        # Place of interest sign
        if enc == 0x2318: continue
        i2font.selection.select(('less', None), enc)
    # Enclosed Alphanumerics
    for enc in range(0x2460, 0x24FF + 1):
        # Circled digit one ... Circled number twenty,
        # Circled digit zero ... Negative circled digit zero
        if (enc >= 0x2460 and enc <= 0x2473) or \
           (enc >= 0x24EA and enc <= 0x24FF): continue
        i2font.selection.select(('less', None), enc)
    # Dingbats
    for enc in range(0x2700, 0x27BF + 1):
        # Dingbat negative circled digit one ...
        #   Dingbat negative circled sans-serif number ten
        if enc >= 0x2776 and enc <= 0x2793: continue
        i2font.selection.select(('less', None), enc)
    # CJK Symbols and Punctuation
    i2font.selection.select(('less', None), 0x3005) #Ideographic iteration mark
    # Enclosed CJK Letters and Months
    for enc in range(0x3200, 0x32FF + 1):
        # Circled number twenty one ... circled number thirty five
        if enc >= 0x3251 and enc <= 0x325F: continue
        i2font.selection.select(('less', None), enc)
    # copy and paste
    i2font.copy()
    i1font.selection.none()
    for enc in i2font.selection:
        i1font.selection.select(('more', None), enc)
    i1font.paste()

    # remove unnecessary glyphs
    i1font.selection.select(('ranges', None), 0xffe7, 0x10257)
    i1font.clear()

    # tweak the Ascent and Descent
    i1font.hhea_ascent = 3940
    i1font.hhea_descent = -1010

    # change font informations
    family = 'UmePlus Gothic'
    fullname = 'UmePlus Gothic'
    i1font.appendSFNTName('English (US)', 'Family', family)
    i1font.appendSFNTName('English (US)', 'UniqueID', family)
    i1font.appendSFNTName('English (US)', 'Fullname', fullname)
    i1font.appendSFNTName('Japanese', 'Family', family)
    i1font.appendSFNTName('Japanese', 'UniqueID', family)
    i1font.appendSFNTName('Japanese', 'Fullname', fullname)
    i1font.fontname = 'UmePlus-Gothic'
    i1font.familyname = family
    i1font.fullname = fullname

    # save
    i1font.save(oname)

    return 0

# execute
if __name__ == '__main__':
    exit(main(sys.argv))
