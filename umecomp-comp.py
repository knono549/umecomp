#!/usr/bin/fontforge -script
# -*- coding: utf-8 -*-
#
# Copyright (c) 2016-2017 knono549
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
#

import inspect
import os
import sys
import traceback

import fontforge
#import math
#import psMat

import ffutils

##
# @return None
def print_usage():
    print("""usage: fontforge -script %s src1 src2 version dest

  - src1
    UmePlus Gothic font file

  - src2
    Inconsolata font file

  - version
    version string

  - dest
    destination file""" % os.path.basename(sys.argv[0]))
    return

##
# @param aMsg str
# @return None
def print_error(aMsg):
    sys.stderr.write(aMsg)
    frame = inspect.currentframe()
    code = frame.f_code
    sys.stderr.write(" (File \"%s\", line %d, in %s)\n" %
                       (code.co_filename, frame.f_lineno, code.co_name))
    return

##
# @param aArgv list<str>
# @return int
def main(aArgv):
    # command line arguments
    if len(aArgv) < 5:
        print_usage()
        return 0

    args = aArgv[1:]

    i1name = args[0]
    i2name = args[1]
    version = args[2]
    oname = args[3]

    # open font files
    i1font = None
    i2font = None
    try:
        i1font = fontforge.open(i1name)
        i2font = fontforge.open(i2name)
    except:
        print_error('Could not open input fonts')
        traceback.print_exc()
        return 1

    # import Inconsolata
    i2font.selection.all()
    # exclude unnecessary blocks
    # IPA Extensions
    i2font.selection.select(('ranges', 'less'), 0x250, 0x2AF)
    # General Punctuation
    i2font.selection.select(('ranges', 'less'), 0x2000, 0x206F)
    # Superscripts and Subscripts
    i2font.selection.select(('ranges', 'less'), 0x2070, 0x209F)
    # Currency Symbols
    i2font.selection.select(('ranges', 'less'), 0x20A0, 0x20CF)
    # Letterlike Symbols
    i2font.selection.select(('ranges', 'less'), 0x2100, 0x214F)
    # Arrows
    i2font.selection.select(('ranges', 'less'), 0x2190, 0x21FF)
    # Mathematical Operators
    i2font.selection.select(('ranges', 'less'), 0x2200, 0x22FF)
    # exclude glyphs which have width different from UmePlus
    i2table = ffutils.RRefLookupTable(i2font)
    try:
        for i2glyph in i2font.selection.byGlyphs:
            i2enc = i2glyph.encoding
            i1glyph = None
            try:
                i1glyph = i1font[i2enc]
            except:
                continue
            if i2glyph.width == i1glyph.width:
                continue
            # i2glyph may be referenced from other glyphs in the future
            # version of Inconsolata
            rglyphs = i2table.rlookup(i2glyph)
            if len(rglyphs) > 0:
                for rglyph in rglyphs:
                    print(hex(rglyph.encoding))
                raise RuntimeError('This script does not support ' + \
                                     'to exclude the referenced glyph: ' + \
                                     i2name + ': ' + hex(i2enc))
            i2font.selection.select(('less', None), i2enc)
    except RuntimeError as e:
        print_error(str(e))
        return 1
    i2font.copy()
    # select glyphs which overlap with Inconsolata
    i1table = ffutils.RRefLookupTable(i1font)
    try:
        for i2enc in i2font.selection:
            i1font.selection.select(('more', None), i2enc)
            i2glyph = None
            i1glyph = None
            try:
                i2glyph = i2font[i2enc]
                i1glyph = i1font[i2enc]
            except:
                continue
            # i1glyph may be referenced from other glyphs in the future
            # version of UmePlus
            rglyphs = i1table.rlookup(i1glyph)
            if len(rglyphs) > 0:
                raise RuntimeError('This script does not support ' + \
                                     'to delete the referenced glyph: ' + \
                                     i1name + ': ' + hex(i2enc))
    except RuntimeError as e:
        print_error(str(e))
        return 1
    i1font.paste()

    # tweak the Ascent and Descent
    i1font.hhea_ascent = i1font.os2_typoascent
    i1font.hhea_descent = i1font.os2_typodescent

    # change font informations
    family = 'UmeComp'
    fullname = 'UmeComp'
    # add the copyright notice of M+
    cright = "[UmeFont]\n" + i1font.copyright + "\n\n" + "[M+ FONTS]\n" + \
      "Copyright(c) 2016 M+ FONTS PROJECT\n\n" + "[Inconsolata]\n" + \
      i2font.copyright
    venurl = 'https://bitbucket.org/knono549/umecomp'
    lictxt = \
"""This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at:
http://scripts.sil.org/OFL"""
    licurl = 'http://scripts.sil.org/OFL'

    i1codepages = i1font.os2_codepages
    i2codepages = i2font.os2_codepages
    i1font.os2_codepages = (i1codepages[0] | i2codepages[0],
                            i1codepages[1] | i2codepages[1])
    i1unicoderanges = i1font.os2_unicoderanges
    i2unicoderanges = i2font.os2_unicoderanges
    i1font.os2_unicoderanges = \
      (i1unicoderanges[0] | i2unicoderanges[0],
       i1unicoderanges[1] | i2unicoderanges[1],
       i1unicoderanges[2] | i2unicoderanges[2],
       i1unicoderanges[3] | i2unicoderanges[3])

    elang = 'English (US)'
    i1sfntnames = i1font.sfnt_names
    i1sntable = dict()
    for i1sfntname in i1sfntnames:
        lang = i1sfntname[0]
        if lang != elang: continue
        i1sntable[i1sfntname[1]] = i1sfntname[2]
    i1font.sfnt_names = \
      ((elang, 'Copyright', cright),
       (elang, 'Family', family),
       (elang, 'SubFamily', i1sntable['SubFamily']),
       (elang, 'UniqueID', fullname),
       (elang, 'Fullname', fullname),
       (elang, 'Version', version),
       (elang, 'Trademark', family),
       (elang, 'Vendor URL', venurl),
       (elang, 'License', lictxt),
       (elang, 'License URL', licurl))

    i1font.fontname = family
    i1font.familyname = family
    i1font.fullname = fullname
    i1font.version = version
    i1font.copyright = cright

    # save
    i1font.save(oname)

    return 0

# execute
if __name__ == '__main__':
    exit(main(sys.argv))
