#
# -*- coding: utf-8 -*-
#
# Copyright (c) 2016 knono549
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
#

##
# reverse lookup table of reference of a glyph
class RRefLookupTable(object):
    ##
    # @param font fontforge.font
    def __init__(self, font):
        ## fontforge.font
        self._font = font
        ## bool
        self._tableInit = False
        ## dict<int, list<int>>
        self._table = dict()

    ##
    # cache reverse lookup table
    # @return None
    def _initTable(self):
        if self._tableInit: return

        self._table.clear()

        ttable = dict() #dict<int, set<int>>

        glyphs = self._font.glyphs()
        try:
            while True:
                glyph = glyphs.next()

                refs = glyph.references
                if len(refs) == 0: continue

                for ref in refs:
                    sglyph = self._font[ref[0]]
                    rglyphs = ttable.get(sglyph.encoding)
                    if not rglyphs:
                        rglyphs = set()
                        ttable[sglyph.encoding] = rglyphs
                    rglyphs.add(glyph.encoding)
        except StopIteration:
            pass

        for enc, rglyphs in ttable.iteritems():
            self._table[enc] = sorted(rglyphs)

    ##
    # @param glyph fontforge.glyph
    # @return list<fontforge.glyph> glyphs which reference to the glyph
    def rlookup(self, glyph):
        self._initTable()

        ret = list()
        encs = self._table.get(glyph.encoding)
        if encs:
            for enc in encs:
                ret.append(self._font[enc])
        return ret
