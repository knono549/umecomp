#!/usr/bin/fontforge -script
# -*- coding: utf-8 -*-
#
# Copyright (c) 2016 knono549
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
#

import inspect
import os
import sys
import traceback

import fontforge
#import math
#import psMat

import ffutils

##
# @return None
def print_usage():
    print("""usage: fontforge -script %s src1 src2 dest

  - src1
    Inconsolata font file

  - src2
    UmePlus Gothic font file

  - dest
    destination sfd file""" % os.path.basename(sys.argv[0]))
    return

##
# @param aMsg str
# @return None
def print_error(aMsg):
    sys.stderr.write(aMsg)
    frame = inspect.currentframe()
    code = frame.f_code
    sys.stderr.write(" (File \"%s\", line %d, in %s)\n" %
                       (code.co_filename, frame.f_lineno, code.co_name))
    return

##
# @param aArgv list<str>
# @return int
def main(aArgv):
    # command line arguments
    if len(aArgv) < 4:
        print_usage()
        return 0

    args = aArgv[1:]

    i1name = args[0]
    i2name = args[1]
    oname = args[2]

    # open font files
    i1font = None
    i2font = None
    try:
        i1font = fontforge.open(i1name)
        i2font = fontforge.open(i2name)
    except:
        print_error('Could not open input fonts')
        traceback.print_exc()
        return 1

    # evacuate some glyphs and update references
    i1table = ffutils.RRefLookupTable(i1font)
    encs = [0xA8, 0xB4]
    for enc in encs:
        glyph = None
        try:
            glyph = i1font[enc]
        except:
            continue

        nglyph = i1font.createChar(-1, glyph.glyphname + '.orig')
        i1font.selection.select(glyph)
        i1font.copy()
        i1font.selection.select(nglyph)
        i1font.paste()

        rglyphs = i1table.rlookup(glyph)
        for rglyph in rglyphs:
            refs = rglyph.references
            rglyph.references = ()
            for ref in reversed(refs):
                if ref[0] == glyph.glyphname:
                    rglyph.addReference(nglyph.glyphname, ref[1])
                else:
                    rglyph.addReference(ref[0], ref[1])

    # set the same em size as UmePlus
    i1font.em = i2font.em

    # remove old data tables and regenerate hints and instructions
    i1font.setTableData('cvt', None)
    i1font.setTableData('fpgm', None)
    i1font.setTableData('prep', None)
    i1glyphs = i1font.glyphs()
    try:
        while True:
            i1glyph = i1glyphs.next()
            i1glyph.autoHint() #with this order
            i1glyph.autoInstr()
    except StopIteration:
        pass

    # set the same Ascent and Descent as UmePlus
    i1font.ascent = i2font.ascent
    i1font.descent = i2font.descent
    i1font.os2_winascent = i2font.os2_winascent
    i1font.os2_windescent = i2font.os2_windescent
    i1font.os2_typoascent = i2font.os2_typoascent
    i1font.os2_typodescent = i2font.os2_typodescent
    i1font.hhea_ascent = i2font.hhea_ascent
    i1font.hhea_descent = i2font.hhea_descent

    # save
    i1font.save(oname)

    return 0

# execute
if __name__ == '__main__':
    exit(main(sys.argv))
